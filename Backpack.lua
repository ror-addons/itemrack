ItemRack.Backpack={};

function ItemRack.Backpack.Init()
	ItemRack.Backpack.oldOnShown=EA_Window_Backpack.OnShown;
	EA_Window_Backpack.OnShown=ItemRack.Backpack.OnShown;
	
	ItemRack.Backpack.oldUpdateAllIconViewSlots=EA_Window_Backpack.UpdateAllIconViewSlots;
	EA_Window_Backpack.UpdateAllIconViewSlots=ItemRack.Backpack.UpdateAllIconViewSlots;
end

function ItemRack.Backpack.OnShown(...)
	ItemRack.Backpack.oldOnShown(... )
	ItemRack.Backpack.ShowUsedItems();
end

function ItemRack.Backpack.UpdateAllIconViewSlots(...)
	ItemRack.Backpack.oldUpdateAllIconViewSlots(... )
	ItemRack.Backpack.ShowUsedItems();
end

function ItemRack.Backpack.ShowUsedItems()
	if(ItemRack.Sets.Active~=true or WindowGetShowing("CharacterWindow")~=true) then
		return;
	end	
	
    local inv=GetInventoryItemData ()
    
    for i,item in ipairs(inv) do	
    	local window,g,index=ItemRack.Backpack.SlotWindowName(i);
    	if(window) then
    		if(ItemRack.Sets.IsUsed(item.uniqueID)) then
				ActionButtonGroupSetTintColor(g,index, 180, 80, 80 )
			else
				ActionButtonGroupSetTintColor(g,index, 255, 255, 255 )
			end
		end
	end	
end

--siehe EA_Window_Backpack.UpdateIconViewSlot
function ItemRack.Backpack.SlotWindowName(slot)
    local pocketNumber     = EA_Window_Backpack.GetPocketNumberForSlot( slot )
    local pocketWindowName = EA_Window_Backpack.GetPocketName( pocketNumber )
    
    local buttonGroupWindowName = pocketWindowName.."Buttons"
    if(EA_Window_Backpack.pockets[pocketNumber]==nil) then
    	return(nil);
    end
    
    local buttonIndex = slot - EA_Window_Backpack.pockets[pocketNumber].firstSlotID + 1
    
    return buttonGroupWindowName.."Button"..buttonIndex,buttonGroupWindowName,buttonIndex;
end