ItemRack V1.13
Resized CharacterWindowTabs to 70% of original size du to compatibility with new rvrstatsupdated

ItemRack V1.12
warhammer patch 1.1a fix
several small fixes

ItemRack V1.11
performance fix
only usable items are shown 
unwanted dependencies fixed

ItemRack V1.1
fixed conflict only with rvrstats not with other addons which are using an additional tab on characterwindow
items which are used in sets are tinted red in bag when itemrackwindow is open
fixed error when combatstats is not installed
 
ItemRack V1.0
its possible to give the set a name

ItemRack V0.2
now able to manage item-sets and link this sets to tactic-sets


ItemRack V0.1
Start of development