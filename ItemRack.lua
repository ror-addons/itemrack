ItemRack = {}
ItemRack.Version=1.13
ItemRack.equipmentData = {}
ItemRack.Window="ItemRackEQShow1";

function ItemRack.Initialize()
	RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "ItemRack.Equipped");
	RegisterEventHandler(SystemData.Events.ENTER_WORLD, "ItemRack.Init_Late");
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "ItemRack.Init_Late");

--	WindowRegisterCoreEventHandler

    ItemRack.oldMouseOverSlot=CharacterWindow.MouseOverSlot;  
    CharacterWindow.MouseOverSlot=ItemRack.MouseOverSlot;

    ItemRack.oldOnShown=CharacterWindow.OnShown;  
    CharacterWindow.OnShown=ItemRack.OnShown;
 
    ItemRack.oldOnHidden=CharacterWindow.OnHidden;  
    CharacterWindow.OnHidden=ItemRack.OnHidden;
    
--	LibSlash.RegisterSlashCmd("itemrack", function(...) ItemRack.Slash(...) end)
	
   	CreateWindow(ItemRack.Window, false)

	if(CharacterWindow.AddExtraTab==nil) then
		CharacterWindow.AddExtraTab=ItemRack.AddExtraTab
		CharacterWindow.FindExtraTab=ItemRack.FindExtraTab
	end   	
	
	ItemRack.Backpack.Init();
	ItemRack.Sets.Init();
	
	TextLogAddEntry("Chat", 0, L"ItemRack v"..towstring(ItemRack.Version)..L" is started");
end

function ItemRack.ShrinkTabs()
	for _,w in ipairs(CharacterWindow.ExtraTabs) do
		WindowSetScale(w,0.7)
	end
end

function ItemRack.AddExtraTab(window)
	if(CharacterWindow.ExtraTabs==nil) then
		CharacterWindow.ExtraTabs={};
		table.insert(CharacterWindow.ExtraTabs,"CharacterWindowTabsCharTab");	
		table.insert(CharacterWindow.ExtraTabs,"CharacterWindowTabsBragsTab");	
		table.insert(CharacterWindow.ExtraTabs,"CharacterWindowTabsTimeoutTab");	
	end
	local n=table.getn(CharacterWindow.ExtraTabs);

   	WindowClearAnchors(window)
   	WindowAddAnchor(window, "topleft", CharacterWindow.ExtraTabs[n], "topright", 0, 0)
  	WindowSetParent(window, "CharacterWindowTabs" )	
  	
  	table.insert(CharacterWindow.ExtraTabs,window);
end

function ItemRack.FindExtraTab(window)
	if(not CharacterWindow.ExtraTabs) then
		return;
	end
	
	for _,name in ipairs(CharacterWindow.ExtraTabs) do
		if(name==window) then
			return(true);
		end
	end
	return(false);
end

function ItemRack.Init_Late()
    ItemRack.CheckOtherAddons()
	ItemRack.ShrinkTabs();
end

function ItemRack.CheckOtherAddons()
	if(RvRStatsRvRTab~=nil and CharacterWindow.FindExtraTab("CharacterWindowTabRvRStats")==false) then
		CharacterWindow.AddExtraTab("CharacterWindowTabRvRStats")
	end
end

function ItemRack.OnUpdate()
	if(string.sub(SystemData.MouseOverWindow.name,1,8)~="ItemRack" and
       string.sub(SystemData.MouseOverWindow.name,1,36)~="CharacterWindowContentsEquipmentSlot") then
		ItemRack.CloseAll();
	end
end

function ItemRack.Shutdown()
end

function ItemRack.Slash(cmd)
    if(cmd=="pvp") then

    end
end

function ItemRack.MouseOverSlot(slot,...)
    local itemData = CharacterWindow.equipmentData[slot];
    
    if(itemData.equipSlot==nil or itemData.equipSlot==0) then
        ItemRack.equipSlot=slot;
    else
        ItemRack.equipSlot=itemData.equipSlot;
    end
    
    ItemRack.Slot_Dest=slot;
    ItemRack.UpdateEquipmentData(ItemRack.equipSlot)
    ItemRack.UpdateSlotIcons()
    
    WindowClearAnchors (ItemRack.Window)
    if(slot<=5 or slot==11 or slot==12) then
        WindowAddAnchor (ItemRack.Window, "bottomleft", "CharacterWindowContentsEquipmentSlot"..slot , "topleft", 0, 0);
    else
        WindowAddAnchor (ItemRack.Window, "topright", "CharacterWindowContentsEquipmentSlot"..slot , "topleft", -2, 0);
    end
    WindowSetShowing(ItemRack.Window, true )
    return ItemRack.oldMouseOverSlot(slot,...)
end

function ItemRack.OnHidden(...)
    ItemRack.Sets.Hide()
    ItemRack.CloseAll()
    return ItemRack.oldOnHidden(...)
end

function ItemRack.OnShown(...)
    ItemRack.Sets.Show()
    return ItemRack.oldOnShown(...)
end

function ItemRack.CloseAll()
    WindowSetShowing(ItemRack.Window, false )
end


function ItemRack.UpdateEquipmentData(equipSlot)
    local inv=GetInventoryItemData ()
    
    ItemRack.equipmentData={}
    for i,item in ipairs(inv) do
--    	d(item.equipSlot)
        if(ItemRack.CheckSlot(equipSlot,item) and DataUtils.SlotIsAllowedForItem(equipSlot,item) and DataUtils.PlayerCanUseItem(item)) then
            item.ItemRackSlot=i;
            table.insert(ItemRack.equipmentData,item)
        end
    end
end

-- see CharacterWindow.UpdateSlotIcons()
function ItemRack.UpdateSlotIcons()
    local texture, x, y  = 0, 0, 0
    local tint = CharacterWindow.NORMAL_TINT 
    local count=0;
    
    for slot=1,10 do
        local e=ItemRack.equipmentData[slot];
        if(e) then
            texture, x, y = GetIconData( e.iconNum ) 
            tint = CharacterWindow.NORMAL_TINT
    
            DynamicImageSetTexture(ItemRack.Window.."EquipmentSlot"..slot.."IconBase", texture, x, y )
            WindowSetShowing(ItemRack.Window.."EquipmentSlot"..slot, true )
            count=slot;
        else
            WindowSetShowing(ItemRack.Window.."EquipmentSlot"..slot, false )
        end
    end 
    
	WindowSetDimensions(ItemRack.Window, 75*count, 75)

end

function ItemRack.EquipmentMouseOver()
    ItemRack.Slot_Source=WindowGetId(SystemData.ActiveWindow.name);
    ItemRack.ItemTooltip(ItemRack.Slot_Source);
end  

function ItemRack.EquipmentRButtonDown()
    ItemRack.Slot_Source=WindowGetId(SystemData.ActiveWindow.name);
    local itemData=ItemRack.equipmentData[ItemRack.Slot_Source];
    
    RequestMoveItem( Cursor.SOURCE_INVENTORY, itemData.ItemRackSlot, Cursor.SOURCE_EQUIPMENT, ItemRack.Slot_Dest, 1 )
end

function ItemRack.Equipped()
    ItemRack.UpdateEquipmentData(ItemRack.equipSlot)
    ItemRack.UpdateSlotIcons()
    ItemRack.ItemTooltip(ItemRack.Slot_Source)
    ItemRack.Sets.ClearCache();
    ItemRack.Sets.Draw();
end

function ItemRack.ItemTooltip(slot)
    local itemData=ItemRack.equipmentData[slot];
    if(not itemData) then
        return;
    end
    Tooltips.CreateItemTooltip( itemData, ItemRack.Window.."EquipmentSlot"..slot, Tooltips.ANCHOR_WINDOW_RIGHT, true )   
end

function ItemRack.ServerName()
	local h=ItemRack.tostring(SystemData.Server.Name)..ItemRack.tostring(GameData.Player.name);
	return h;
end

function ItemRack.tostring(ws)
	if(not ws) then
		return(nil);
	end
	
	local s=tostring(ws);
	s=string.gsub(s, "^[%s]*(.-)[%s]*$", "%1");
	s=string.gsub(s, "(%\x19)", " ");
	if(not s) then
		return(nil);
	end
	s=tostring(ItemRack.ParsePattern(s,"[^%^]+"));
	
	return(s);
end

function ItemRack.ParsePattern(text,pat)
	local t={};
	for x in wstring.gmatch(towstring(text),towstring(pat)) do
		table.insert(t,x);
	end
	return unpack(t);
end



function ItemRack.CheckSlot(slot,itemData)
	if(not slot) then
		return(false);
	end
	if(not itemData.equipSlot) then
		return(false);
	end
	if(slot==itemData.equipSlot) then
		return(true);
	end
	if(slot==1 and itemData.equipSlot==4) then
		return(true);
	end	
	if(slot==2 and itemData.equipSlot==4) then
		return(true);
	end	
	if(slot>=17 and slot<=20 and itemData.equipSlot==17) then
		return(true);
	end	
end