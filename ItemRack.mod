﻿<?xml version="1.0" encoding="UTF-8"?>

<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="ItemRack" version="1.13" date="12/14/2008" >

		<Author name="Realwild@helmgart" email="" />
        
		<Description text="ItemRack" />

		<Dependencies>
		        <Dependency name="EA_CharacterWindow" />
		</Dependencies>

		<Files>
			<File name="ItemRack.xml" />
			<File name="Tactics.xml" />
			<File name="Sets.xml" />
			<File name="Backpack.lua" />
		</Files>

	        <SavedVariables>
            		<SavedVariable name="ItemRack.Sets.Data" />
       		 </SavedVariables>

		<OnInitialize>
			<CallFunction name="ItemRack.Initialize" />
		</OnInitialize>

		<OnUpdate>
			<CallFunction name="ItemRack.OnUpdate" />
		</OnUpdate>

		<OnShutdown>
			<CallFunction name="ItemRack.Shutdown" />
		</OnShutdown>

	</UiMod>

</ModuleFile>    