ItemRack.Sets={}
ItemRack.Sets.Data={};
ItemRack.Sets.Set=nil;
ItemRack.Sets.Ref={};
ItemRack.Sets.MaxSet=0;
ItemRack.Sets.Index={};
ItemRack.Sets.Found=nil;

function ItemRack.Sets.Init()

	
    ItemRack.Sets.oldUpdateMode=CharacterWindow.UpdateMode;  
    CharacterWindow.UpdateMode=ItemRack.Sets.UpdateMode;
 
    ItemRack.Sets.oldEquipmentLButtonDown=CharacterWindow.EquipmentLButtonDown;  
    CharacterWindow.EquipmentLButtonDown=ItemRack.Sets.EquipmentLButtonDown;
 
   	CreateWindow("CharacterWindowTabsItemRack", true)
    ButtonSetText( "CharacterWindowTabsItemRack", L"ItemRack" )
	CharacterWindow.AddExtraTab("CharacterWindowTabsItemRack")
	
   	CreateWindow("ItemRackSets", false)
   	WindowSetParent( "ItemRackSets", "CharacterWindow" )
    ButtonSetText("ItemRackSetsSave", L"Save")
    ButtonSetText("ItemRackSetsNew", L"New")
    ButtonSetText("ItemRackSetsDelete", L"Delete")

    ItemRack.Tactics.Initialize();

    ItemRack.Sets.Set=ItemRack.Sets.InitSet();
    
    if(ItemRack.Sets.Data[ItemRack.ServerName()]==nil) then
        ItemRack.Sets.Data[ItemRack.ServerName()]={}
    end
    
    ItemRack.Sets.BuildRef();
    
 end



function ItemRack.Sets.UpdateMode(...)
    ItemRack.Sets.SetActive(false)
	ItemRack.Sets.Draw()
	
    ButtonSetPressedFlag( "CharacterWindowTabsItemRack", false )
    ButtonSetStayDownFlag( "CharacterWindowTabsItemRack", true )
            
    return ItemRack.Sets.oldUpdateMode(...)
end


function ItemRack.Sets.EquipmentLButtonDown(...)
    if(ItemRack.Sets.Active==false) then
        return ItemRack.Sets.oldEquipmentLButtonDown(...)
    else
        ItemRack.Sets.ToggleSlot()    
    end
end

function ItemRack.Sets.ClearCache()
	ItemRack.Sets.Found=nil;
end

function ItemRack.Sets.Show()
	if( ItemRack.Sets.Active==true) then
	   ItemRack.Sets.OnTabSelect()   
	end
end

function ItemRack.Sets.Hide()
	EA_Window_Backpack.UpdateAllIconViewSlots()
end

function ItemRack.Sets.SetActive(v)
    local nv=not v;
    
    ItemRack.Sets.Active=v;

	WindowSetShowing("ItemRackSets", v )

	EA_Window_Backpack.UpdateAllIconViewSlots()

    ButtonSetPressedFlag( "CharacterWindowTabsCharTab", nv )
    ButtonSetStayDownFlag( "CharacterWindowTabsCharTab", v )
    ButtonSetPressedFlag( "CharacterWindowTabsBragsTab", nv )
    ButtonSetStayDownFlag( "CharacterWindowTabsBragsTab", v )
    ButtonSetPressedFlag( "CharacterWindowTabsItemRack", v )
    ButtonSetStayDownFlag( "CharacterWindowTabsItemRack", nv )
    
    
	WindowSetShowing("CharacterWindowContentsStatsWindow1", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow2", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow3", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow4", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow5", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow6", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow7", nv )
	WindowSetShowing("CharacterWindowContentsStatsWindow8", nv )
	WindowSetShowing("CharacterWindowContentsStatCombobox", nv )
end



function ItemRack.Sets.OnTabSelect()
  
    CharacterWindow.UpdateMode(CharacterWindow.MODE_NORMAL)
      
    ItemRack.Sets.SetActive(true)
    ItemRack.Sets.ComboBoxBuild()
    ItemRack.Sets.Load(ItemRack.Sets.FindSet())
    ItemRack.Sets.Draw();
end

function ItemRack.Sets.ComboBoxBuild()
    ComboBoxClearMenuItems("ItemRackSetsCombobox")
    for _,i in ipairs(ItemRack.Sets.Index) do
            ComboBoxAddMenuItem("ItemRackSetsCombobox", L"("..towstring(i)..L")"..towstring(ItemRack.Sets.Data[ItemRack.ServerName()][i].Name))
    end
end

function ItemRack.Sets.Select(index)
    if(index==nil or index==0) then
        return;
    end
    local set=ItemRack.Sets.Index[index];
    
    ItemRack.Sets.Load(set)
    ItemRack.Sets.Draw()
end

function ItemRack.Sets.Equip(slot,uniqueID)
    local source=ItemRack.Sets.FindItemInBag(uniqueID)
    if(source~=nil) then
        RequestMoveItem( Cursor.SOURCE_INVENTORY, source, Cursor.SOURCE_EQUIPMENT, slot, 1 )
    end
    local source=ItemRack.Sets.FindItemInEquipment(uniqueID)
    if(source~=nil) then
        RequestMoveItem( Cursor.SOURCE_EQUIPMENT, source, Cursor.SOURCE_EQUIPMENT, slot, 1 )
    end
end

function ItemRack.Sets.FindItemInBag(uniqueID)
    local inv=GetInventoryItemData ()
    for slot,item in ipairs(inv) do
        if(uniqueID==item.uniqueID) then
            return(slot)
        end
    end    
    return(nil);
end

function ItemRack.Sets.FindItemInEquipment(uniqueID)
    local ed=GetEquipmentData()
    for slot,item in ipairs(ed) do
        if(uniqueID==item.uniqueID) then
            return(slot)
        end
    end    
    return(nil);
end

function ItemRack.Sets.InitSet(num)
    local set={};
    
    if(num~=nil and num~=0) then
        set.Name="Set "..num;
    end
    
    set.Items={};
    set.Active={};

    return(set)
end

function ItemRack.Sets.Load(set)
    if(ItemRack.Sets.Data[ItemRack.ServerName()][tostring(set)]==nil) then
        ItemRack.Sets.Set=ItemRack.Sets.InitSet();
    else    
        ItemRack.Sets.Set=ItemRack.Sets.Data[ItemRack.ServerName()][tostring(set)];
    end

    local ed=GetEquipmentData()
    for slot,item in ipairs(ed) do
        if(ItemRack.Sets.Set.Active[tostring(slot)]==true) then
            if(ItemRack.Sets.Set.Items[tostring(slot)]~=item.uniqueID) then
                ItemRack.Sets.Equip(slot,ItemRack.Sets.Set.Items[tostring(slot)])
            end
        end
    end  
    
    ItemRack.Sets.SetSelected(set)
    ItemRack.Tactics.Select(ItemRack.Sets.Set.Tactics)
end

function ItemRack.Sets.ButtonSave()
    ItemRack.Sets.Save(ItemRack.Sets.GetSelected())
end

function ItemRack.Sets.ButtonNew()
    local num=ItemRack.Sets.NewNum();
    ItemRack.Sets.Data[ItemRack.ServerName()][tostring(num)]=ItemRack.Sets.InitSet(num);
    ItemRack.Sets.BuildIndex()
    ItemRack.Sets.ComboBoxBuild()
    ItemRack.Sets.Load(num)
    ItemRack.Sets.Draw();
end

function ItemRack.Sets.NewNum()
    for i=1,ItemRack.Sets.MaxSet do
        if(ItemRack.Sets.Data[ItemRack.ServerName()][tostring(i)]==nil) then
            return(i)
        end
    end
    ItemRack.Sets.MaxSet=ItemRack.Sets.MaxSet+1;
    return(ItemRack.Sets.MaxSet)
end

function ItemRack.Sets.ButtonDelete()
    local num=ItemRack.Sets.GetSelected()
    ItemRack.Sets.Data[ItemRack.ServerName()][tostring(num)]=nil;
    ItemRack.Sets.BuildIndex()
    ItemRack.Sets.ComboBoxBuild()
    ItemRack.Sets.Load(ItemRack.Sets.FindSet())
    ItemRack.Sets.Draw();
end

function ItemRack.Sets.Save(set)
    ItemRack.Sets.Set.Items={};
  
    local ed=GetEquipmentData()
    for slot,item in ipairs(ed) do
        if(ItemRack.Sets.Set.Active[tostring(slot)]==true) then
            ItemRack.Sets.Set.Items[tostring(slot)]=item.uniqueID;  
        end      
    end
    
    ItemRack.Sets.Set.Name=ItemRackSetsName.Text;
    ItemRack.Sets.Data[ItemRack.ServerName()][tostring(set)]=ItemRack.Sets.Set;
    ItemRack.Sets.BuildRef();
    ItemRack.Sets.ComboBoxBuild();
    ItemRack.Sets.SetSelected(set);
    ItemRack.Sets.ClearCache();
end

function ItemRack.Sets.ToggleSlot()
    local slot = WindowGetId(SystemData.ActiveWindow.name)

    ItemRack.Sets.Set.Active[tostring(slot)]=not ItemRack.Sets.Set.Active[tostring(slot)];
    
    ItemRack.Sets.DrawSlot(slot)
end

function ItemRack.Sets.Draw()
    local ed=GetEquipmentData()
    for slot,item in ipairs(ed) do
        if(slot==4) then
            continue;
        end
        if(slot==15) then
            continue;
        end
        if(slot==16) then
            continue;
        end
        ItemRack.Sets.DrawSlot(slot)  
        ItemRack.Tactics.Draw()  
        if(ItemRack.Sets.Set.Name) then
       	    TextEditBoxSetText( "ItemRackSetsName", towstring(ItemRack.Sets.Set.Name) )
       	else
       	    TextEditBoxSetText( "ItemRackSetsName", L"" )
       	end
    end
end

function ItemRack.Sets.DrawSlot(slot)
    if(ItemRack.Sets.Active~=true or ItemRack.Sets.Set.Active[tostring(slot)]==true) then
        WindowSetTintColor( "CharacterWindowContentsEquipmentSlot"..slot, 255, 255, 255 )
    else
        WindowSetTintColor( "CharacterWindowContentsEquipmentSlot"..slot, 80, 80, 80 )
    end
end

function ItemRack.Sets.GetSelected()
    local set=0;
    local index=ComboBoxGetSelectedMenuItem("ItemRackSetsCombobox");
    if(index~=0 and ItemRack.Sets.Index[index]~=nil) then
       set=ItemRack.Sets.Index[index];
    end
    if(set==0) then 
        set=ItemRack.Sets.FindSet();
        ItemRack.Sets.SetSelected(set);
    end
    return set;
end

function ItemRack.Sets.SetSelected(set)
    if(set~=nil and set~=0) then
        ComboBoxSetSelectedMenuItem("ItemRackSetsCombobox", ItemRack.Sets.Data[ItemRack.ServerName()][tostring(set)].num)
    end
end

function ItemRack.Sets.FindSet()
	if(ItemRack.Sets.Found) then
		return(ItemRack.Sets.Found);
	end
	
    local h=0;
    local set=0;

    for i,_ in pairs(ItemRack.Sets.Data[ItemRack.ServerName()]) do
        local v=ItemRack.Sets.Eval(i)
        if(v>h) then
            set=i;
            h=v;
        end
    end
    
   	ItemRack.Sets.Found=tonumber(set);
    
    return(ItemRack.Sets.Found)
end

function ItemRack.Sets.Eval(set)
    local count=0;
    local hit=0;
    
    local ed=GetEquipmentData()    
    for slot,uniqueID in pairs(ItemRack.Sets.Data[ItemRack.ServerName()][set].Items) do
        if(ItemRack.Sets.Data[ItemRack.ServerName()][set].Active[slot]==true) then
            count = count + 1;
            if(ed[tonumber(slot)].uniqueID==uniqueID) then
                hit=hit+1;
            end
        end
    end
    if(count==0) then
        return(0);
    end
    
    return(hit/count)
end

function ItemRack.Sets.BuildRef()
    ItemRack.Sets.BuildRef_1();
    ItemRack.Sets.BuildIndex();
end

function ItemRack.Sets.BuildIndex()
    ItemRack.Sets.Index={}
    for i=1,ItemRack.Sets.MaxSet do
        if(ItemRack.Sets.Data[ItemRack.ServerName()][tostring(i)]~=nil) then
            table.insert(ItemRack.Sets.Index,tostring(i))
            ItemRack.Sets.Data[ItemRack.ServerName()][tostring(i)].num=table.getn(ItemRack.Sets.Index)
        end
    end
end

function ItemRack.Sets.BuildRef_1()
    for i,set in pairs(ItemRack.Sets.Data[ItemRack.ServerName()]) do
        i=tonumber(i);
        if(i>ItemRack.Sets.MaxSet) then
            ItemRack.Sets.MaxSet=i;
        end
        for slot,uniqueID in pairs(set.Items) do
            if(set.Active[slot]==true) then
                ItemRack.Sets.Ref[tostring(uniqueID)]=true;
            end
        end
    end
end

function ItemRack.Sets.IsUsed(uniqueID)
    return(ItemRack.Sets.Ref[tostring(uniqueID)])
end