ItemRack.Tactics={}
ItemRack.Tactics.Max=5;

function ItemRack.Tactics.Initialize()
    ComboBoxAddMenuItem ("ItemRackSetsSetMenu", L"");

    for i = 1, ItemRack.Tactics.Max do
        ComboBoxAddMenuItem ("ItemRackSetsSetMenu", towstring(i));
    end
    
	LabelSetText("ItemRackSetsText1", L"Tactic set");
end

function ItemRack.Tactics.TacticsSetMenuTooltip()
    local i = WindowGetId(SystemData.ActiveWindow.name)
    
    if(i==0 or i>ItemRack.Tactics.Max) then
        return;
    end
    
    TacticsSetTooltip:CreateTooltip(i-1);
    Tooltips.CreateCustomTooltip (SystemData.ActiveWindow.name, TacticsSetTooltip.windowName);
    Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_RIGHT);
end

function ItemRack.Tactics.OnSetMenuSelectionChanged()
    local i=ComboBoxGetSelectedMenuItem("ItemRackSetsSetMenu");
    
    d(i)
    if(i>=2) then
        ItemRack.Sets.Set.Tactics=i-1;
    else
        ItemRack.Sets.Set.Tactics=nil;
    end
end

function ItemRack.Tactics.Select(n)
    if(not n) then
        return;
    end
    TacticsEditor.OnSetMenuSelectionChanged(n)
end

function ItemRack.Tactics.Draw()
    if(ItemRack.Sets.Set.Tactics) then
        ComboBoxSetSelectedMenuItem("ItemRackSetsSetMenu", ItemRack.Sets.Set.Tactics+1)
    else
        ComboBoxSetSelectedMenuItem("ItemRackSetsSetMenu", 0)
    end
end